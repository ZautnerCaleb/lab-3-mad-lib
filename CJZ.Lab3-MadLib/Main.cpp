#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

int const numQuestions = 11;
string questions[numQuestions] = {"a Name", "an Adjective", "a Silly Word", "a Noun (plural)", "a Noun", "a Noun (Plural)", "a Verb ending in \"ed", "a Noun", "a Noun", "a Verb ending in \"ed", "an Adjective"};
string answers[numQuestions];
string results[numQuestions] = { "Darth ", " looked at his master while his ", " breathing filled the room. He was told to go to ", "everything on the planet of ", ". He got in his ", "and jumped to hyperspace. Soon before he reached the planet, he dropped out of hyperspace and was attaked by Rebel ", ". He ", " them off and continued to the planet's surface. He landed and confronted more opposition, slicing it down with his ", ". He used the ", " to choke another Rebel, then ", " him aside. He finished off all life on the planet with a/an " };

void collection(string question[numQuestions], string answer[numQuestions])
{
	for (int i = 0; i < numQuestions; i++)
	{
		cout << "Please enter " << question[i] << ": ";
		cin >> answer[i];
	}
}

string display(string answer[numQuestions])
{
	string results = "Darth " + answer[0] + " looked at his master while his " + answer[1] + " breathing filled the room. He was told to go to planet " + answer[2] + ", the planet of " + answer[3] + ". He got in his " + answer[4] + " and jumped to hyperspace. Soon before he reached the planet, he dropped out of hyperspace and was attaked by Rebel " + answer[5] + ". He " + answer[6] + " them off and continued to the planet's surface. He landed and confronted more opposition, slicing it down with his " + answer[7] + ". He used the " + answer[8] + " to choke another Rebel, then " + answer[9] + " him aside. He finished off all life on the planet with a/an " + answer[10] + " laugh.";
	//cout << "Darth " << answer[0] << " looked at his master while his " << answer[1] << " breathing filled the room. He was told to go to planet " << answer[2] << ", the planet of " << answer[3] << ". He got in his " << answer[4] << " and jumped to hyperspace. Soon before he reached the planet, he dropped out of hyperspace and was attaked by Rebel " << answer[5] << ". He " << answer[6] << " them off and continued to the planet's surface. He landed and confronted more opposition, slicing it down with his " << answer[7] << ". He used the " << answer[8] << " to choke another Rebel, then " << answer[9] << " him aside. He finished off all life on the planet with a/an " << answer[10] << " laugh.";
	return results;
}


int main()
{
	
	collection(questions, answers);
	cout << "\n";
	cout << display(answers);
	cout << "\n";
	cout << "\n";
	cout << "Would you like to save your results? [Y for yes N for no] ";
	char save;
	cin >> save;
	if (save == 'y')
	{
		string path = "madlib.txt";
		
		ofstream ofs(path);
		ofs << display(answers);
		ofs.close();

	}

	_getch();
	return 0;

}